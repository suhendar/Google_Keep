<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'auth'],function(){
		Route::get('/', function () {
    		return redirect('/');
		});
		Route::get('/','keepController@front');

		
		// Route::post('/notebooks','keepController@store');
		// Route::get('/notebooks/create','keepController@create');
		// Route::get('/notebooks/{notebooks}','keepController@show')->name('notebooks.show');		
		// Route::get('/notebooks/{notebooks}/edit','keepController@edit')->name('notebooks.edit');
		// Route::put('/notebooks/{notebooks}','keepController@update');
		// Route::delete('/notebooks/{notebooks}','keepController@destroy');

		Route::resource('notebooks','keepController');
		Route::resource('notes','NotesController');


		Route::get('notes/{notebookId}/createNote','NotesController@createNote')->name('notes.createNote');
});

// Route::get('/keep','keepController@index');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
