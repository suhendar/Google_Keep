<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notebook extends Model
{
	protected $connection = 'pgsql';

	protected $table = 'note';

	protected $fillable=['name'];

	public function notes()
	{
		return $this->hasMany(Note::class);
	}
}
