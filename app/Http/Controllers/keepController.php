<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Notebook;
use Illuminate\Http\Request;
/**
* 
*/
class keepController extends BaseController
{
	
	// function index()
	// {
	// 	return view('keep\index');
	// }

	function front()
	{
		return view('.frontpage');
	}

	public function index()
	{
		$user= Auth::user();
		$note=$user->notebooks;

    	return view('notebooks.index', compact('note'));
	}

	public function create()
	{
		return view('notebooks.create');
	}

	public function show($id)
	{
		$note= Notebook::findOrFail($id);
		$notes= $note->notes;
		return view('notes.index',compact('notes','note'));
	}

	public function store(Request $request)
	{
		$dataInput=$request->all();
		$user=Auth::user();
		$user->notebooks()->create($dataInput);
		// Notebook::create($dataInput);

		//return "berhasil";
		return redirect('/notebooks');
	}

	public function edit($id)
	{
		$user= Auth::user();
		$note= $user->notebooks()->find($id);
		// $note=Notebook::where('id',$id)->first();
		return view('notebooks.edit')->with('notebook',$note);
	}

	public function update(Request $request,$id)
	{
		// $note=Notebook::where('id',$id)->first();
		$user= Auth::user();
		$note= $user->notebooks()->find($id);

		$note->update($request->all());

		return redirect('/notebooks');
	}

	public function destroy($id)
	{
		// $note=Notebook::where('id',$id)->first();
		$user= Auth::user();
		$note= $user->notebooks()->find($id);

		$note->delete();
		return redirect('/notebooks');
	}
}