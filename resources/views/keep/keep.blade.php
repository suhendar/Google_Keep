<html>

	<head>
		<title>Gokeep</title>
		
		<script src="js/layout.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/layout.css"/>
		</noscript>
	</head>

	<body>
		<div class="notes" style="position: relative; height: 1238px";>
			<div class="notes" style="position: absolute; left: 0px; top: 0px"> 
				<div class="note-inner" style="height: 310px"></div>
			</div>
		</div>

		<button class="big-add show-add-note"><i class="material-icons">add</i></button>
	</body>
</html>